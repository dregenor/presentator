var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    browserify = require('gulp-browserify'),
    exec = require('gulp-exec'),
    bump = require('gulp-bump'),
    sequence = require('run-sequence');


gulp.task('build', function (cb) {
    return gulp.src('js/src/index.js')
        .pipe(browserify({
            insertGlobals: true,
            debug: false
        }))
        .pipe(uglify({
            output:{
                ascii_only:true
            },
            compress:{
                sequences     : true,  // join consecutive statemets with the “comma operator”
                properties    : true,  // optimize property access: a["foo"] → a.foo
                dead_code     : true,  // discard unreachable code
                drop_debugger : true,  // discard “debugger” statements
                unsafe        : true, // some unsafe optimizations (see below)
                conditionals  : true,  // optimize if-s and conditional expressions
                comparisons   : true,  // optimize comparisons
                evaluate      : true,  // evaluate constant expressions
                booleans      : true,  // optimize boolean expressions
                loops         : true,  // optimize loops
                unused        : true,  // drop unused variables/functions
                hoist_funs    : true,  // hoist function declarations
                hoist_vars    : true, // hoist variable declarations
                if_return     : true,  // optimize if-s followed by return/continue
                join_vars     : true,  // join var declarations
                cascade       : true,  // try to cascade `right` into `left` in sequences
                side_effects  : true,  // drop side-effect-free statements
                warnings      : true   // warn about potentially dangerous optimizations/code
            }
        }))
        .pipe(gulp.dest('./js'));
    
});

gulp.task('releaseJs',function(){
    return gulp.src(['js/index.js','js/data.js'])
        .pipe(gulp.dest('./dist/js'));
});

gulp.task('releaseHtml',function(){
    return gulp.src(['index.html'])
        .pipe(gulp.dest('./dist'));
});



gulp.task('default',['watch']);

gulp.task('bump', function () {
    return gulp.src(['./package.json'])
        .pipe(bump())
        .pipe(gulp.dest('./'));
});

gulp.task('github-release', require('./tasks/github-release').createAndUpload);

gulp.task('zip',function(){
    var version = require('./package.json').version;
    return gulp.src('').pipe(exec('cd dist && zip -r ../dist_' + version + '.zip ./ && cd ..', {
        silent: true,
        continueOnError: true
    }));
});

gulp.task('release',['releaseJs','releaseHtml','bump'],function(cb){
    sequence('zip','github-release',cb)
});

gulp.task('watch',function(){

    gulp.watch(
        ['js/src/**'],
        {
            interval:100,
            debounceDelay:50
        }, 
        ['build']
    );
    
});
