

//var data = require('./data.json');

var Presentator = require('./Presentator');
var JM = require('./jsonMapper'),
    H = JM.helpers,
    F = require('./fetures');
//
//{
//			"file": "2.mp4",
//			"loop": false,
//			"pause": [9, 20]
//		}
//
//{
//                "name":"first looped Slide",
//                "actions":{
//                    "onEnd":"loop",
//                    "onKeyRight":"next",
//                    "onKeyLeft":"prev"
//                },
//                "source":"video/1.2.mp4"
//                "cues":[
//                    {
//                        "start":3,
//                        "end":3,
//                        "action":"pause"
//                    }
//                ]
//            }

var converter = JM.makeConverter({
    'name': H.def(''),
    'actions':{
        "onKeyRight":H.def("nextMarker"),
        "onKeyLeft":H.def("prevMarker"),
        "onKeyRightPaused":H.def("nextMarker"),
        "onKeyLeftPaused":H.def("prevMarker"),
        "onEnd":function(input){        
            return input.loop?
                        "loop":
                        "next";
        }
    },
    'fade':'fade',
    'fadeOut':'fadeOut',
    'source': ['file',H.template('video/{$root}')],
    'pauseLength':'pauseLength',
    "cues":function(input){
        if(F.isArray(input.pause)){
            var pl = input.pauseLength;
            return input.pause.map(function(itm){
                return {
                    "start": function (tm) {
                        var val = parseInt(tm, 10);
                        if (val == 0) {
                            val = val + 0.001;
                        }
                        return val;
                    }(itm),
                    "end": function (tm) {
                        var val = parseInt(tm, 10);
                        if (val == 0) {
                            val = val + 0.001;
                        }
                        return val + 0.04;
                    }(itm),
                    "action": function (tm) {
                        if(typeof pl !== 'undefined'){
                            return ['resumeAfter',pl];
                        } else {
                            return 'pause';
                        }
                    }(itm)
                }
            })
        }
    }
});

var mapConverter = JM.makeMapConverter(converter);

$(document).ready(function(){
    var data = window.scenario||{};
    
    if(F.isArray(data)){
        data = {
            name:'',
            slides:mapConverter(data)
        };
    }
    console.log(data);
    
    $('#play').on('click',function(){
        $('#play').hide();
        new Presentator(data,$('#presentator'));  
        var html = document.documentElement;
        fullScreen(html);
        $('#presentator').focus();
    });    
});

function fullScreen(element) {
  if(element.requestFullscreen) {
    element.requestFullscreen();
  } else if(element.webkitRequestFullscreen) {
    element.webkitRequestFullscreen();
  } else if(element.mozRequestFullscreen) {
    element.mozRequestFullScreen();
  }
}