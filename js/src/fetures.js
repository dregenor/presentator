module.exports = {
    isArray:function(itm){
        return Array.isArray(itm)
    },
    isObject:function isObject(value){
      // http://jsperf.com/isobject4
      return value !== null && typeof value === 'object';
    }
}