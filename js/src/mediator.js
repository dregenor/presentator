/**
 * 
 * @constructor
 */
var Mediator = function(){
    this.targets = [];
};

Mediator.prototype.subscribe = function(target){
    this.targets.push(target);
};

Mediator.prototype.unSubscribe = function(target){
    var i = this.targets.indexOf(target);
    if(i>=0){
        this.targets.splice(i,1);
    }
};


Mediator.prototype.emit = function(action,data){
    this.targets.forEach(function(target){
        if(target['actions'] && target['actions'][action]){
            target['actions'][action].call(target,data);
        }
    })
};

module.exports = new Mediator();