var mediator = require('./mediator');
var F = require('./fetures');
var Slide = function(data,target,index){
    this.__index = index;
    this.data = data;
    mediator.subscribe(this);
    this.init(target);
    this.bindActions(target);
    this.bufferingData();
};  

Slide.prototype.init = function(target){
    this.createView(target);
};

Slide.prototype.bindActions = function(){
    this.actions = {};
    
    this._view.video.on('loadeddata',function(){
        this.ready = true;
        mediator.emit('ready',this);
    }.bind(this));
    
    this._view.video.on('ended',this.onEnded.bind(this));
    
};

Slide.prototype.onEnded = function(evt){
    if(this.data.actions['onEnd']){
        mediator.emit(this.data.actions['onEnd']);    
    }
};
Slide.prototype.onKeyRight = function(evt){
    if(this._state == 'pause'){
        mediator.emit(this.data.actions['onKeyRightPaused'])    
    }else{
        mediator.emit(this.data.actions['onKeyRight']);
    }
};
Slide.prototype.onKeyLeft = function(evt){
    if(this._state == 'pause'){
        mediator.emit(this.data.actions['onKeyLeftPaused'])    
    }else{
        mediator.emit(this.data.actions['onKeyLeft']);
    }
};

Slide.prototype.createView = function(target){
    this._view = {
        root:$('<div class="slide"></div>'),
        video:$('<video preload="none"></video>')
    };
    
    this._view.root.toggle(false);
    this._view.root.append(this._view.video);  
    
    if(this.data.fade){
        this._view.fade = $('<div class="fade"></div>');
        this._view.root.append(this._view.fade);  
    }

    if(this.data.fadeOut){
        this._view.fadeOut = $('<div class="fade-dark"></div>');
        this._view.root.append(this._view.fadeOut);
    }
    
    target.append(this._view.root);
};

Slide.prototype.bufferingData = function(){
    // подставляем сорсец в видео  
    
    this._view.video.append('<source src="'+ this.data.source +'" type="video/mp4">')
    this._view.video[0].load();
    if(this.data.cues && this.data.cues.length){
        var track = this._view.video[0].addTextTrack('metadata');
        this.data.cues.forEach(function(cueData){
            var cue = new VTTCue(cueData.start, cueData.end, '');
            track.addCue(cue);
            cue.onenter = function(){
                if(F.isArray(cueData.action)){
                    mediator.emit.apply(mediator,cueData.action);
                } else {
                    mediator.emit(cueData.action);
                }
            }.bind(this);
        }.bind(this))        
    }
};

Slide.prototype.show = function(pause){
    this._view.root.toggle(true);
    !pause && (this._view.video[0].currentTime = 0);
    !pause && this.play();
    !pause && this.data.fadeOut && this.fadeOut();
    
    $('html').on('keydown.'+this.__index,this.handleKeys.bind(this));  
};

Slide.prototype.handleKeys = function(e) {
    switch(e.which) {
        case 37: // left
            e.preventDefault();
            this.onKeyLeft();
        break;

        case 32: // space
            e.preventDefault();
            if (this.data.fade){
                this.fade();
            } else {
                this.play();
            }
        break;

        case 82:
            mediator.emit('restartAll');
        break;
        
        case 13: // enter
            e.preventDefault();
            this.pause();
        break;        
        
        case 39: // right
            e.preventDefault();
            this.onKeyRight();
        break;

        default: return; 
    }
    
};

Slide.prototype.hide = function(){
    this._view.root.toggle(false);
    //this._view.video[0].currentTime = 0;
    this._view.video[0].pause();
    $('html').off('keydown.'+this.__index);
    this._state = 'hidden';
};

Slide.prototype.pause = function(){
    this._view.video[0].pause();
    this._state = 'pause';
};

Slide.prototype.togglePause = function(){
    if(this._state == 'pause'){
        this.play();
    } else {
        this.pause();    
    }
};

Slide.prototype.fade = function(){
    var w = parseInt(this._view.video.width(),10);
    this._view.fade.width(w);
    this._view.fade.css({"margin-left": -w/2});
    
    
    this._view.fade.toggleClass('fadeIn',true);
    setTimeout(function(){
        mediator.emit('next');
        this._view.fade.toggleClass('fadeIn',false);
        this._view.fadeOut.toggleClass('fade-dark-out',false);
    }.bind(this),2000);
};


Slide.prototype.fadeOut = function(){

    var w = parseInt(this._view.video.width(),10);
    this._view.fadeOut.width(w);
    this._view.fadeOut.css({"margin-left": -w/2});


    this._view.fadeOut.toggleClass('fade-dark-out',true);
};

Slide.prototype.play = function(){
    this._view.video[0].play();
    this._state = 'play';
}; 

Slide.prototype.seek = function(pos){
    this._view.video[0].currentTime = pos;

};

Slide.prototype.pos = function(){
    return this._view.video[0].currentTime;
    
}

module.exports = Slide;