var F = require('./fetures');
var Slide = require('./slide');
var mediator = require('./mediator');

/**
 *
 * @param {Array} data
 * @param target
 */
var Presentator = function(data,target){
    this['actions'] = {
        'ready':this.onReadySlide.bind(this),
        'next':this.next.bind(this),
        'nextMarker':this.nextMarker.bind(this),
        'prevMarker':this.prevMarker.bind(this),
        'loop':this.loop.bind(this),
        'prev':this.prev.bind(this),
        'pause':this.pause.bind(this),
        'resumeAfter':this.resumeAfter.bind(this),
        'resume':this.resume.bind(this),
        'restartAll':this.restartALL.bind(this)
    };
    
    this.markers = markers = [];
    
    if(F.isObject(data)){
        
        var counter = 0;

        if(F.isArray(data.slides)){
            this.slides = data.slides.map(function(slideData,index){
                
                slideData.cues && slideData.cues.forEach(function(cue){
                    cue.index = counter++;
                    cue.slideIndex = index;
                    markers.push(cue);
                }.bind(this));
                
                return new Slide(slideData,target,index);

                
            }.bind(this))
        }
    }
    mediator.subscribe(this);
};

Presentator.prototype.onReadySlide = function(slide){
    var allReady = true;
    this.slides.forEach(function(slide){
        allReady = allReady && slide.ready;
    });
    if(allReady){
       this.play(); 
    }
};

Presentator.prototype.play = function(){
    this.currentSlide = 0;
    this.slides[this.currentSlide].show();
    
   
};

Presentator.prototype.next = function(){
    this.slides[this.currentSlide].hide();

    this.currentSlide++;
    if(this.currentSlide >= this.slides.length ){
        this.currentSlide = 0;
    }
    this.slides[this.currentSlide].show();
};

Presentator.prototype.prev = function(){
    this.slides[this.currentSlide].hide();
    this.currentSlide--;
    if(this.currentSlide < 0 ){
        this.currentSlide = this.slides.length - 1;
    }
    this.slides[this.currentSlide].show();
};

Presentator.prototype.loop = function(){
    this.slides[this.currentSlide].show();
};

Presentator.prototype.pause = function(){
    this.slides[this.currentSlide].pause();
};

Presentator.prototype.resumeAfter = function(timeout){
    this.slides[this.currentSlide].pause();
    setTimeout(function(){
        this.slides[this.currentSlide].play();
    }.bind(this),timeout * 1000)
};


Presentator.prototype.resume = function(){
    this.slides[this.currentSlide].play();
};

Presentator.prototype.findCurrentMarker = function(){
    
    var slideIndex = this.currentSlide;
    var pos = this.slides[this.currentSlide].pos();
    var result = this.markers.filter(function(marker){
        return marker.slideIndex == slideIndex && marker.start <= pos && marker.end >=pos;
    });
    return result.length >0? result[0]: false;
};

Presentator.prototype.findNextMarker = function(){
    
    var slideIndex = this.currentSlide;
    var pos = this.slides[this.currentSlide].pos();
    
    var result = this.markers.filter(function(marker){
        return marker.slideIndex == slideIndex && marker.start > pos || 
               marker.slideIndex > slideIndex;
    });
    if (result.length >0){
        return result[0];
    } else {
        return {index:this.markers.length};
    }
   
};

Presentator.prototype.findPrevMarker = function(){
    
    var slideIndex = this.currentSlide;
    var pos = this.slides[this.currentSlide].pos();
    
    var result = this.markers.filter(function(marker){
        return (marker.slideIndex == slideIndex && marker.end < pos) || 
               marker.slideIndex < slideIndex;
    });
    if (result.length >0){
        return result[result.length-1];
    } else {
        return {index:-1};
    }
};


Presentator.prototype.nextMarker = function(){
    
    // по любому мы встаем в паузу

    var currentMarker = this.findCurrentMarker(),
        index;
    

    if(currentMarker){
        index = currentMarker.index +1;    
    } else {
        index = this.findNextMarker().index;
    }
    // маркер не последний
    
    if(this.markers.length <= index ){
        return ;//2) запрещаем переход с последнего стопа на первый по нажатию на "вправо"
        //index = 0;
    }

    this.slides[this.currentSlide].pause();

    var newMarker = this.markers[index],
        buffIndex;
    
    if(newMarker.slideIndex != this.currentSlide){
        buffIndex = newMarker.slideIndex;
        this.slides[buffIndex].seek(newMarker.end);

        // dirty hack
        this.slides[buffIndex]._view.video[0].addEventListener('seeked',handle)
        
        var self = this;
        function handle(){
            self.slides[self.currentSlide].hide();
            self.slides[buffIndex].show(true);        
            self.currentSlide = buffIndex;    
            self.slides[buffIndex]._view.video[0].removeEventListener('seeked',handle)
        }
    } else {

        this.slides[this.currentSlide].seek(newMarker.end);
    }
    
};

Presentator.prototype.prevMarker = function(){


    // маркер не последний
    var currentMarker = this.findCurrentMarker(),
        index;

    if(currentMarker){
        index = currentMarker.index -1;    
    } else {
        index = this.findPrevMarker().index;
    }
    
    if( index < 0){

        return; //fix 3) запрещаем переход с первого стопа на последний по нажатию на "влево"
        //index = this.markers.length -1;
    }

    // по любому мы встаем в паузу
    this.slides[this.currentSlide].pause();
    var newMarker = this.markers[index];
    var buffIndex;
    if(newMarker.slideIndex != this.currentSlide){
        buffIndex = newMarker.slideIndex;
        // dirty hack
        this.slides[buffIndex].seek(newMarker.end);
        this.slides[buffIndex]._view.video[0].addEventListener('seeked',handle)
        
        var self = this;
        function handle(){
            self.slides[buffIndex].show(true);        
            self.slides[self.currentSlide].hide();
            self.currentSlide = buffIndex; 
            self.slides[buffIndex]._view.video[0].removeEventListener('seeked',handle)
        }
    } else {
        this.slides[this.currentSlide].seek(newMarker.end);
    }
};
Presentator.prototype.restartALL = function(){
    this.slides[this.currentSlide].hide();
    this.currentSlide = 0;
    this.slides[this.currentSlide].show();
};

module.exports = Presentator;