(function(){
    // упрощенный вариант сценария
    var scenario = [
        {
			"file": "1.2.mp4",  // название файла, все видео хранятся в папке video
			"loop": true,  // нужно ли закольцовывать это видео
			"fade": true,  // нужно ли делать фэйд
			"fadeOut": true,  // нужно ли делать фэйд
			"pause": []  // перечисление точек для паузы
		},
        {
			"file": "2.mp4",  // название файла, все видео хранятся в папке video
			"loop": false,  // нужно ли закольцовывать это видео
			"pause": [3, 49],  // перечисление точек для паузы
            "pauseLength":'3'
		},
        {
            "file": "1.2.mp4",  // название файла, все видео хранятся в папке video
            "loop": true,  // нужно ли закольцовывать это видео
            "fade": true,  // нужно ли делать фэйд
            "pause": []  // перечисление точек для паузы
        },
		{
			"file": "2.mp4",
			"loop": false,
			"pause": [ 0, 9, 20,49]
		}
    ];
    window.scenario = scenario;
})();




//    // сложный вариант сценария
//    var scenario = {
//        "name":"Example Presentation",
//        "slides":[
//            {
//                "name":"first looped Slide",
//                "actions":{
//                    "onEnd":"loop",
//                    "onKeyRight":"next",
//                    "onKeyLeft":"prev"
//                },
//                "source":"video/1.2.mp4"
//            },{
//                "name":"Second ended Slide",
//                "actions":{
//                    "onEnd":"next",
//                    "onKeyRight":"nextMarker",
//                    "onKeyLeft":"prevMarker",
//                    "onKeyRightPaused":"resume"
//                },
//                "cues":[
//                    {
//                        "start":3,
//                        "end":3,
//                        "action":"pause"
//                    }
//                ],
//                "source":"video/2.mp4"
//            }
//        ]
//    };